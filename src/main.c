#include <string.h>

#include "array.h"
#include "data.h"
#include "struct.h"

struct actor {
	struct cell *cell;
	struct wire_group *group;
	struct control *control;
};

static void init_group_current(struct wire_group *group)
{
	group->current.charge = {
		.marks = NULL,
		.marks_a = array_init(),
	};
	group->current.pending_charge = {
		.marks = NULL,
		.marks_a = array_init(),
	};
	group->current.iteration = 0;
	group->current.filter_iteration = 0;
}

// TODO: Merge this function and the previous one.
static void init_wire_project_current(struct wire_project *project)
{
	project->current.charge = {
		.marks = NULL,
		.marks_a = array_init(),
	};
	project->current.pending_charge = {
		.marks = NULL,
		.marks_a = array_init(),
	};
	project->current.iteration = 0;
	project->current.filter_iteration = 0;
}

static void init_control_current(struct control *control)
{
	// XXX: do we need this?
	control->current.visited = false;
}

static void reset_wire_charge(struct wire_charge *charge)
{
	charge->marks_a.length = 0;
}

static struct mark *get_mark(char *name, struct wire_group *group, bool pending)
{
	struct mark **marks_ptr;
	struct array *array_ptr;
	if(pending) {
		marks_ptr = &group->current.charge.marks;
		array_ptr = &group->current.charge.marks_a;
	} else {
		marks_ptr = &group->current.pending_charge.marks;
		array_ptr = &group->current.pending_charge.marks_a;
	}

	size_t len = array_ptr->len;
	struct mark *marks = *marks_ptr;
	for(size_t i = 0; i < len; i++)
		if(!strcmp(marks[i].name, name))
			return &marks[i];

	struct mark *res;
	*(res = array_add(array_ptr, marks_ptr, 1)) = (struct mark){
		.name = name,
		.filtered = false,
		.data = {
			.type = MarkNumber,
			.number = { .value = 0 },
		}
	};

	return res;
}

struct mark_data merge_marks(struct mark_data first, struct mark_data second)
{
	if(first.type != second.type)
		return (struct mark_data){ .type = MarkEmpty };

	switch(first.type) {
	case MarkAnchor:
	case MarkDig:
	case MarkEmpty:
	case MarkMerge:
		return first;
		break;

	case MarkNumber:
		return (struct mark_data){ .type = MarkNumber,
			.number = { .value = first.number.value + second.number.value },
		}
		break;
	}

	panic(false);
	return (struct mark_data){};
}

static void get_merge_mark(char *name, struct wire_group *group, bool pending, struct mark_data merge)
{
	struct mark *mark = get_mark(name, group, pending);
	mark->data = merge_marks(mark->data, merge);
}

static void add_group_actors(struct array *actors_a, struct actor **actors,
		struct cell *cell, struct wire_group *group)
{
	struct actor *new_actors = array_add(actors_a, actors, group->controls_len);
	for(int i = 0; i < group->controls_len; i++) {
		new_actors[i] = (struct actor){
			.cell = cell,
			.group = group,
			.control = &group->controls[i],
		};
	}
}

// Add all actors that would cause a differen't group's actors to be travelled
// to. To be used after the iteration has been increased so that everything
// flows were it should.
// This may cause duplicates in the list of actors, but it's fine because the
// second to be processed would be a no-op since it would have had it's
// iteration changed already.
// TODO: What do we do about shards?
static void add_travel_actors(struct array *actors_a, struct actor **actors,
		struct cell *cell, struct wire_group *group)
{
	for(int i = 0; i < group->controls_len; i++) {
		if(group->controls[i].type == CtrlBridge ||
				group->controls[i].type == CtrlProject) {
			*array_add(actors_a, actors, 1) = (struct actor){
				.cell = cell,
				.group = group,
				.control = &cur.group->controls[i],
			};
		}
	}
}

static bool test_try(struct test *test, struct wire_group *group, struct cell *cell)
{
	int number;
	switch(test->type) {
	// TODO: fix
	// TODO: add filter logic
	break; case TestMark: number = get_mark(test->mark, group, false)->value;
	break; case TestGoal: number = grid[cell->x][cell->y].goal;
	}

	return (number >= test->number) == test->ge;
}

// Returns the new filter id.
static int mark_filtered_cells(structe cell *cell, struct wire_group *group,
		struct control *ctrl, int filter_iteration, char *mark)
{
	struct array actors_a = array_init();
	struct actor *actors = NULL;

	struct array next_actors_a = array_init();
	struct actor *next_actors = NULL;

	group->current.filter_iteration = filter_iteration;

	// TODO: Ensure this function continues to be correct if it's modified later.
	add_travel_actors(&next_actors_a, &next_actors, cell, group);
	// TODO: finish
	for(int dist = 0; next_actors_a.len > 0 &&
			(ctrl->filter.max_dist == -1 || dist <= ctrl->filter.max_dist); dist++) {
		// Swap the actors lists and reset the new next_actors.
		struct array tmp_a = actors_a;
		struct actor *tmp = actors;
		actors_a = next_actors_a;
		actors = next_actors;
		next_actors_a = tmp_a;
		next_actors = tmp;
		next_actors_a.len = 0;

		while(actors_a.len > 0) {
			struct actor cur = actors[0];
			struct control *ctrl = cur.control;
			array_rm(&actors_a, &actors, &actors[0]);

			bool fits_guard = true;
			for(int j = 0; j < ctrl->guard_len; j++) {
				if(!test_try(&ctrl->guard[j], cur.group, cur.cell)) {
					filter_guard = false;
					break;
				}
			}

			if(fits_guard) {
				switch(ctrl->type) {
				case CtrlBridge:
					// TODO: Make a function to search for this.
					for(int i = 0; i < cur.cell->groups_len; i++) {
						struct wire_group *group = &cur.cell->groups[i];
						if(group->id != ctrl->bridge.group)
							continue;
						if(group->current.filter_iteration >= filter_iteration)
							break;
						group->current.filter_iteration = filter_iteration;

						add_travel_actors(&actors_a, &actors, cur.cell, group);

						break;
					}
				case CtrlProject:
					// TODO: add_travel_actors into next_actors.
				}
			}
			
			// TODO: Set filtered to true by default.
			if(ctrl->filter.min_dist != -1 && dist < ctrl->filter.min_dist)
				continue;
			int j;
			for(j = 0; j < ctrl->filter.tests_len; j++)
				if(!test_try(&ctrl->filter.tests[i], cur.group, cur.cell))
					break;
			if(j < ctrl->tests_len)
				continue;

			// TODO: set filtered to false if it wasn't already set to true
			// TODO: (for the mark)
		}
	}

	for(int i = 0; i < GW; i++) {
		for(int j = 0; j < GH; j++) {
			if(grid[i][j].type == ObjCell) {
				for(int k = 0; k < grid[i][j].cell.groups_len; k++) {
					if(grid[i][j].cell.groups[k].current.filter_iteration < filter_iteration) {
						// TODO: set filtered to true
					}
				}
			}
		}
	}

	free(actors);
	free(next_actors);
}

static void heartbeat()
{
	int iteration = 1;
	int filter_iteration = 1;

	// Reset the per-heartbeat state of each cell.
	for(int i = 0; i < GW; i++) {
		for(int j = 0; j < GH; j++) {
			if(grid[i][j].type == ObjCell) {
				grid[i][j].cell.project_down.current.iteration = 0;
				grid[i][j].cell.project_down.current.filter_iteration = 0;
				reset_wire_charge(&grid[i][j].cell.project_down.current.charge);
				reset_wire_charge(&grid[i][j].cell.project_down.current.pending_charge);
				grid[i][j].cell.project_down.controls1_a.len = 0;
				grid[i][j].cell.project_down.controls2_a.len = 0;

				grid[i][j].cell.project_right.current.iteration = 0;
				grid[i][j].cell.project_right.current.filter_iteration = 0;
				reset_wire_charge(&grid[i][j].cell.project_right.current.charge);
				reset_wire_charge(&grid[i][j].cell.project_right.current.pending_charge);
				grid[i][j].cell.project_right.controls1_a.len = 0;
				grid[i][j].cell.project_right.controls2_a.len = 0;

				for(int k = 0; k < grid[i][j].cell.groups_len; k++) {
					grid[i][j].cell.groups[k].current.iteration = 0;
					grid[i][j].cell.groups[k].current.filter_iteration = 0;
					reset_wire_charge(&grid[i][j].cell.groups[k].current.charge);
					reset_wire_charge(&grid[i][j].cell.groups[k].current.pending_charge);
				}
			}
		}
	}
	for(int i = 0; i < GW; i++) {
		for(int j = 0; j < GH; j++) {
			if(grid[i][j].type == ObjCell) {
			}
		}
	}

	struct array actors_a = array_init();
	struct actor *actors = NULL;

	for(int i = 0; i < heart.init_groups_len; i++) {
		heart.init_groups[i]->current.iteration = 1;
		add_group_actors(&actors_a, &actors, heart.cell, heart.init_groups[i]);
	}

	while(actors_a.len > 0) {
		// TODO: Move pending charges over to regular charges.
		// TODO: Turn MarkEmpty into MarkNumber with a value of zero.
		do {
			int handled = 0;
			for(int i = actors_a.len - 1; i >= 0; i--) {
				struct actor cur = actors[i];
				struct control *ctrl = cur.control;

				// Test against the guard and skip if the test fails.
				int j;
				for(j = 0; j < ctrl->guard_len; j++)
					if(!test_try(&ctrl->guard[j], cur.group, cur.cell))
						break;
				if(j < ctrl->guard_len)
					continue;

				// We passed the guard, so we can remove this now, because it's handled.
				array_rm(&actors_a, &actors, &actors[i]);
				handled += 1;

				switch(ctrl->type) {
					case CtrlAnchor:
						get_merge_mark(ctrl->anchor.mark, cur.group, true, (struct mark_data){
							.type = MarkAnchor,
						});
						iteration += 1;
						add_travel_actors(&actors_a, &actors, cur.cell, cur.group);
						break;

					case CtrlBridge:
						for(int i = 0; i < cur.cell->groups_len; i++) {
							struct wire_group *group = &cur.cell->groups[i];
							if(group->id != ctrl->bridge.group)
								continue;
							if(group->current.iteration >= iteration)
								break;

							group->current.iteration = iteration;
							add_group_actors(&actors_a, &actors, cur.cell, group);

							// Merge our group's charge into the bridged group's.
							struct wire_charge *src = &cur.group->current.pending_charge;
							struct wire_charge *dest = &group->current.pending_charge;
							for(int i = 0; i < src->marks_a.len; i++) {
								struct mark *mark = get_mark(src->marks[i].name, group, true);
								mark->data = merge_marks(src->marks[i].data, mark->data);
							}

							break;
						}

						break;

					case CtrlDig:
						get_merge_mark(ctrl->anchor.mark, cur.group, true, (struct mark_data){
							.type = MarkDig,
						});
						iteration += 1;
						add_travel_actors(&actors_a, &actors, cur.cell, cur.group);
						break;

					case CtrlMark:
						break;

					case CtrlMerge:
						get_merge_mark(ctrl->anchor.mark, cur.group, true, (struct mark_data){
							.type = MarkDig,
						});
						iteration += 1;
						add_travel_actors(&actors_a, &actors, cur.cell, cur.group);

						break;

					case CtrlPhysPortal:

						break;

					case CtrlProject:

						break;

					case CtrlShard:

						break;

				}
			}
		} while(handled > 0);
	}

	free(actors);
}

int main()
{
	data();

	init_wire_project_current(&heart.cell->project_down);
	init_wire_project_current(&heart.cell->project_right);
	for(int i = 0; i < heart.cell->groups_len; i++) {
		init_group_current(&heart.cell->groups[i]);
		for(int j = 0; j < heart.cell->groups[i].controls_len; j++)
			init_control_current(&heart.cell->groups[i].controls[j]);
	}

	heartbeat();
}
