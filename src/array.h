#ifndef ARRAY_H
#define ARRAY_H

#include <stdlib.h>
#include <string.h>

struct array {
	size_t len;
	size_t cap;
};

static inline size_t array_add_size(struct array *array, void **ptr, size_t nmemb, size_t size)
{
	size_t res = array->len;
	array->len += nmemb;
	if(array->len > array->cap) {
		array->cap = (array->cap==0)?1024:(array->cap*2);
		*ptr = realloc(*ptr, array->cap * size);
	}
	return res;
}

static inline size_t array_remove_size(struct array *array, void **ptr, void *remove, size_t size)
{
	memcpy(remove, *ptr + (--array->len) * size, size);
}

#define array_init() \
	((struct array){ .len = 0, .cap = 0 })

// Add n items to the array, returning a pointer to the new items of the type *ptr.
#define array_add(array, ptr, n) \
	((array_add_size(array, (void **)(ptr), (n), sizeof(**(ptr))), *(ptr)) + (array)->len - n)

// Swap remove of the array item pointed to by rm.
#define array_rm(array, ptr, rm) \
	(*(rm) = (*(ptr))[--(array)->len])

#endif // ARRAY_H
