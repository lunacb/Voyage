#!/usr/bin/env ruby
# TODO: update for the new filters.

GX = 50
GY = 50
GW = 100
GH = 100

heart = {
  "next_group" => 0,
}

cell = {
  "x" => GX.to_s,
  "y" => GY.to_s,
  "groups" => ["struct wire_group", []],
  "paths" => "{ NULL, NULL, NULL, NULL }",
  "heart" => "&heart",
  "current" => { "visited" => "false" },
}

init_groups = []
group = nil
control = nil
control_spec = nil

def make_test(args)
  case args[0]
  when "mark"
    return {
      "type" => "TestMark",
      "ge" => (args[2][0] == ">").to_s,
      "number" => ((args[2][0] == ">") == (args[2][1] == "="))?(args[3]):((args[3].to_i + 1).to_s),
      "mark" => "\"#{args[1]}\"",
    }
  when "goal"
    return {
      "type" => "TestGoal",
      "ge" => "true",
      "number" => "1",
    }
  end
  puts "WHAT #{args}"
end

File.open(ARGV[0], "r") do |f|
  f.each_line(chomp: true).filter_map { |a| a.split if !a.empty? and a !~ /^\s*#/  }.each do |a|
    case a[0]
    when "heart"
    when "init"
      init_groups.push a[2]

    when "group"
      group = {
        "id" => a[1],
        "controls" => ["struct control", []],
      }
      cell["groups"][1].push group

    when "project", "mark", "dig", "merge", "anchor", "shard", "bridge"
      control_spec = {}
      control = {
        "type" => "Ctrl#{a[0].capitalize}",
        "guard" => [ "struct test", [] ],
        "filter" => {
          "min_dist" => "-1",
          "max_dist" => "-1",
          "tests" => [ "struct test", [] ],
        },
        a[0] => control_spec
      }
      group["controls"][1].push control

    when "phys"
      control_spec = {}
      control = {
        "type" => "CtrlPhysPortal",
        "guard" => [ "struct test", [] ],
        "filter" => {
          "min_dist" => "-1",
          "max_dist" => "-1",
          "tests" => [ "struct test", [] ],
        },
        "phys_portal" => control_spec
      }
      group["controls"][1].push control

    when "guard"
      control["guard"][1].push make_test(a[1..])

    when "filter"
      if a[1] == "distance" then
        case a[2]
        when ">="
          control["filter"]["min_dist"] = a[3]
        when ">"
          control["filter"]["min_dist"] = (a[3].to_i + 1).to_a
        when "<="
          control["filter"]["max_dist"] = a[3]
        when "<"
          control["filter"]["min_dist"] = (a[3].to_i - 1).to_a
        when "=="
          control["filter"]["min_dist"] = a[3]
          control["filter"]["max_dist"] = a[3]
        end
      else
        control["filter"]["tests"][1].push make_test(a[1..])
      end
    when "link"
      control_spec["mark_#{a[1]}"] = "\"#{a[2]}\""
    else
      puts "WARNING: unkown command \"#{a.join(" ")}\""
    end

    # Extra contorol initialization.
    case a[0]
    when "project"
      control_spec["dir"] = "FDir#{a[1].upcase}"
    when "anchor", "dig", "mege"
      control_spec["mark"] = "\"#{a[1]}\""
    when "shard"
      control_spec["group"] = a[4]
      control_spec["mark"] = "\"#{a[1]}\""
    when "phys"
      control_spec["my_dir"] = "PDir#{a[4].capitalize}"
      control_spec["anchor_dir"] = "PDir#{a[2].capitalize}"
      control_spec["mark"] = "\"#{a[3]}\""
    when "bridge"
      control_spec["group"] = a[3]
    when "dig"
      control_spec["mark"] = "\"#{a[1]}\""
    when "merge"
      control_spec["mark"] = "\"#{a[1]}\""
    end
  end
end

$id = (1..).each

def render(data, depth, arrays)
  if data.class == Hash then
    res = "{\n"
    res += data.each_pair.map do |a,b|
      c = "#{"\t" * (depth + 1)}.#{a} = #{render(b, depth + 1, arrays)}"
      c += ",\n#{"\t" * (depth + 1)}.#{a}_len = #{b[1].length}" if b.class == Array
      c
    end.join(",\n")
    res += "\n#{"\t" * depth}}"
    return res
  elsif data.class == Array then
    if data[1].empty? then
      return "NULL"
    else
      name = "_array_#{$id.next}"
      array = "static #{data[0]} #{name}[] = {\n"
      array += data[1].map { |a| "\t" * 1 + render(a, 1, arrays) }.join(",\n")
      array += "\n};"
      arrays.push array
      return name
    end
  else
    return data.to_s
  end
end

arrays = []
cell_text = render(cell, 0, arrays)
heart_text = render(heart, 0, arrays)
arrays_text = arrays.join("\n\n")

init_groups = init_groups.map do |a|
  i = cell["groups"][1].find_index { |b| b["id"] == a }
 "&grid[#{GX}][#{GY}].cell.groups[#{i}]"
end
init_groups_array_name = "_array_#{$id.next}"
init_groups_text = init_groups.each_with_index.map do |a,i|
  "#{init_groups_array_name}[#{i}] = #{a};"
end.join("\n")

contents = <<EOF
//  Generated by gendata.rb

#ifndef DATA_H
#define DATA_H

#include "array.h"
#include "struct.h"

#define GW #{GW}
#define GH #{GW}

static struct heart heart = #{heart_text};

#{arrays_text}

static struct obj grid[GW][GH] = { [50] = { [50] = (struct obj){ .type = ObjCell, .damage = 0, .cell = #{cell_text} } } };

static struct wire_group * #{init_groups_array_name}[#{init_groups.length}];

static void data(void)
{
  heart.cell = &grid[#{GX}][#{GY}].cell;
  #{init_groups_text}
  heart.init_groups_len = #{init_groups.length};
}

#endif // DATA_H
EOF

File.open(ARGV[1], "w") { |f| f.puts contents }
