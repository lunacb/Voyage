#ifndef STRUCT_H
#define STRUCT_H

#include <stdbool.h>
#include <stdio.h>

#include "array.h"

enum phys_dir {
	PDirN = 0,
	PDirS,
	PDirE,
	PDirW,
	PDirUp,
	PDirDown,

	PDirLast,
};

enum fabric_dir {
	FDirN = 0,
	FDirS,
	FDirE,
	FDirW,
	FDirUp,
	FDirDown,

	FDirLast,
};

enum control_type {
	CtrlAnchor,
	CtrlBridge,
	CtrlDig,
	CtrlFilter,
	CtrlMark,
	CtrlMerge,
	CtrlPhysPortal,
	CtrlProject,
	CtrlShard,
};

enum test_type {
	TestMark,
	// TODO
	TestGoal,
};

enum obj_type {
	ObjNone = 0,
	ObjCell,
};

enum mark_type {
	MarkAnchor,
	MarkDig,
	MarkEmpty,
	MarkMerge,
	MarkNumber,
};

typedef char * markid_t;
typedef int groupid_t;

struct mark_data {
	enum mark_type type;
	union {
		struct {
		} empty;

		struct {
			int value;
		} number;

		struct {
		} dig;

		struct {
		} merge;

		struct {
		} anchor;
	}
};

struct mark {
	char *name;
	// If true, the mark for this group doesn't match all the filters applied
	// to it.
	bool filtered;
	struct mark_data data;
};

struct test {
	enum test_type type;
	// If true then testing for greater-than-or-equal; less-than if false.
	bool ge;
	markid_t mark;
	int number;
};

struct control {
	struct test *guard;
	int guard_len;

	enum control_type type;

	union {
		struct {
			markid_t mark;
		} anchor;
		
		struct {
			groupid_t group;
		} bridge;

		struct {
			markid_t mark;
		} dig;

		struct {
			markid_t mark;
			int min_dist, max_dist;
			struct test *tests;
			int tests_len;
		} filter;
		
		struct {
			markid_t mark_true, mark_false;
		} mark;

		struct {
			markid_t mark;
		} merge;

		struct {
			enum phys_dir my_dir, anchor_dir;
			markid_t mark;
		} phys_portal;

		struct {
			enum fabric_dir dir;
		} project;

		struct {
			groupid_t group;
			markid_t mark;
		} shard;
	};

	struct {
		bool visited;
	} current;
};

struct wire_charge {
	struct mark *marks;
	struct array marks_a;
};

struct wire_group {
	groupid_t id;

	struct control *controls;
	int controls_len;

	struct {
		struct wire_charge charge, pending_charge;
		int iteration;
		// For scanning of nodes by filters.
		int filter_iteration;
	} current;
};

// A special wire group inbetween every two adjacent cells.
struct wire_project {
	struct cell *cell1, cell2;
	// Only CtrlBridge.
	struct control *controls1, *controls2;
	struct array controls1_a, cnotrols2_a;

	struct {
		struct wire_charge charge, pending_charge;
		int iteration;
		int filter_iteration;
	} current;
};

struct cell {
	int x, y;

	struct wire_group *groups;
	int groups_len;

	// TODO: When a project control is used, it's like a bridge, except it bridges to one of these.
	// TODO: Especially here, we'll want to be able to specify how to bridge FROM and TO a group, instead of just both ways.
	struct wire_project project_down, project_right;

	struct cell *paths[FDirLast];

	struct heart *heart;
};

struct heart {
	struct cell *cell;

	struct wire_group **init_groups;
	int init_groups_len;

	groupid_t next_group;
};

struct obj {
	int damage;
	bool goal;
	enum obj_type type;
	union {
		struct {
		} none;

		struct cell cell;
	};
};

#endif // STRUCT_H
